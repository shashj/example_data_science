---
title: This is a Knowledge Template Header
authors:
- Shashank_Jatav
tags:
- knowledge
- example
created_at: 2017-11-21
updated_at: 2017-11-21
tldr: This is short description of the content and findings of the post.
---

```{r}
library(readr)
ozone <- read_csv("hourly_44201_2014.csv",col_types = "ccccinnccccccncnncccccc")
names(ozone) <- make.names(names(ozone))
```

```{r}
nrow(ozone)
```

```{r}
ncol(ozone)
```
```{r}
str(ozone)
```

```{r}
head(ozone[, c(6:7, 10)])
```


```{r}
tail(ozone[, c(6:7, 10)])
```

```{r}
head(table(ozone$Time.Local))
```

```{r}
library(plyr)
library(dplyr)

select(ozone, State.Name) %>% unique %>% nrow
```

```{r}
unique(ozone$State.Name)
```

This last bit of analysis made use of something we will discuss in the next section: external data. We knew that there are only 50 states in the U.S., so seeing 52 state names was an immediate trigger that something might be off. In this case, all was well, but validating your data with an external data source can be very useful.

##Validate With at Least One External Data Source

```{r}
summary(ozone$Sample.Measurement)
```

```{r}
quantile(ozone$Sample.Measurement, seq(0, 1, 0.1))
```

##Make a plot

```{r}
par(las = 2, mar = c(10, 4, 2, 2), cex.axis = 0.8)
boxplot(Sample.Measurement ~ State.Name, ozone, range = 0, ylab = "Ozone level (ppm)")
```

```{r}
library(plotly)
plot_ly(ozone, y = ~Sample.Measurement, x = ~State.Name, type = "box")
```

##Returning to the original question (Easy Solution First)

```{r}
library(maps)
{map("state")
abline(v = -100, lwd = 3)
text(-120, 30, "West")
text(-75, 30, "East")}
```

```{r}
ozone$region <- factor(ifelse(ozone$Longitude < -100, "west", "east"))
group_by(ozone, region) %>% summarize(mean = mean(Sample.Measurement, na.rm = TRUE), median = median(Sample.Measurement, na.rm = TRUE))
```

```{r}
boxplot(Sample.Measurement ~ region, ozone, range = 0)
```

##Challenge your solution

```{r}
filter(ozone, State.Name != "Puerto Rico" & State.Name != "Georgia" & State.Name != "Hawaii") %>% group_by(region) %>% summarize(mean = mean(Sample.Measurement, na.rm = TRUE), median = median(Sample.Measurement, na.rm = TRUE))
```



