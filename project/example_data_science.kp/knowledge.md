---
title: This is a Knowledge Template Header
authors:
- Shashank_Jatav
tags:
- knowledge
- example
created_at: 2017-11-21 00:00:00
updated_at: 2017-11-21 10:22:18.137722
tldr: This is short description of the content and findings of the post.
thumbnail: images/unnamed-chunk-12-1.png
---
```r
library(readr)
ozone <- read_csv("hourly_44201_2014.csv",col_types = "ccccinnccccccncnncccccc")
```

```
## Warning: Unnamed `col_types` should have the same length as `col_names`.
## Using smaller of the two.
```

```
## Warning in rbind(names(probs), probs_f): number of columns of result is not
## a multiple of vector length (arg 1)
```

```
## Warning: 9075457 parsing failures.
## row # A tibble: 5 x 5 col     row   col   expected     actual                    file expected   <int> <chr>      <chr>      <chr>                   <chr> actual 1     1  <NA> 23 columns 24 columns 'hourly_44201_2014.csv' file 2     2  <NA> 23 columns 24 columns 'hourly_44201_2014.csv' row 3     3  <NA> 23 columns 24 columns 'hourly_44201_2014.csv' col 4     4  <NA> 23 columns 24 columns 'hourly_44201_2014.csv' expected 5     5  <NA> 23 columns 24 columns 'hourly_44201_2014.csv'
## ... ................. ... ........................................................... ........ ........................................................... ...... ........................................................... .... ........................................................... ... ........................................................... ... ........................................................... ........ ...........................................................
## See problems(...) for more details.
```

```r
names(ozone) <- make.names(names(ozone))
```


```r
nrow(ozone)
```

```
## [1] 9075457
```


```r
ncol(ozone)
```

```
## [1] 23
```

```r
str(ozone)
```

```
## Classes 'tbl_df', 'tbl' and 'data.frame':	9075457 obs. of  23 variables:
##  $ State.Code        : chr  "01" "01" "01" "01" ...
##  $ County.Code       : chr  "003" "003" "003" "003" ...
##  $ Site.Num          : chr  "0010" "0010" "0010" "0010" ...
##  $ Parameter.Code    : chr  "44201" "44201" "44201" "44201" ...
##  $ POC               : int  1 1 1 1 1 1 1 1 1 1 ...
##  $ Latitude          : num  30.5 30.5 30.5 30.5 30.5 ...
##  $ Longitude         : num  -87.9 -87.9 -87.9 -87.9 -87.9 ...
##  $ Datum             : chr  "NAD83" "NAD83" "NAD83" "NAD83" ...
##  $ Parameter.Name    : chr  "Ozone" "Ozone" "Ozone" "Ozone" ...
##  $ Date.Local        : chr  "2014-03-01" "2014-03-01" "2014-03-01" "2014-03-01" ...
##  $ Time.Local        : chr  "01:00" "02:00" "03:00" "04:00" ...
##  $ Date.GMT          : chr  "2014-03-01" "2014-03-01" "2014-03-01" "2014-03-01" ...
##  $ Time.GMT          : chr  "07:00" "08:00" "09:00" "10:00" ...
##  $ Sample.Measurement: num  0.047 0.047 0.043 0.038 0.035 0.035 0.034 0.037 0.044 0.046 ...
##  $ Units.of.Measure  : chr  "Parts per million" "Parts per million" "Parts per million" "Parts per million" ...
##  $ MDL               : num  0.005 0.005 0.005 0.005 0.005 0.005 0.005 0.005 0.005 0.005 ...
##  $ Uncertainty       : num  NA NA NA NA NA NA NA NA NA NA ...
##  $ Qualifier         : chr  NA NA NA NA ...
##  $ Method.Type       : chr  "FEM" "FEM" "FEM" "FEM" ...
##  $ Method.Code       : chr  "047" "047" "047" "047" ...
##  $ Method.Name       : chr  "INSTRUMENTAL - ULTRA VIOLET" "INSTRUMENTAL - ULTRA VIOLET" "INSTRUMENTAL - ULTRA VIOLET" "INSTRUMENTAL - ULTRA VIOLET" ...
##  $ State.Name        : chr  "Alabama" "Alabama" "Alabama" "Alabama" ...
##  $ County.Name       : chr  "Baldwin" "Baldwin" "Baldwin" "Baldwin" ...
##  - attr(*, "problems")=Classes 'tbl_df', 'tbl' and 'data.frame':	9075457 obs. of  5 variables:
##   ..$ row     : int  1 2 3 4 5 6 7 8 9 10 ...
##   ..$ col     : chr  NA NA NA NA ...
##   ..$ expected: chr  "23 columns" "23 columns" "23 columns" "23 columns" ...
##   ..$ actual  : chr  "24 columns" "24 columns" "24 columns" "24 columns" ...
##   ..$ file    : chr  "'hourly_44201_2014.csv'" "'hourly_44201_2014.csv'" "'hourly_44201_2014.csv'" "'hourly_44201_2014.csv'" ...
##  - attr(*, "spec")=List of 2
##   ..$ cols   :List of 23
##   .. ..$ State Code        : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ County Code       : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Site Num          : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Parameter Code    : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ POC               : list()
##   .. .. ..- attr(*, "class")= chr  "collector_integer" "collector"
##   .. ..$ Latitude          : list()
##   .. .. ..- attr(*, "class")= chr  "collector_number" "collector"
##   .. ..$ Longitude         : list()
##   .. .. ..- attr(*, "class")= chr  "collector_number" "collector"
##   .. ..$ Datum             : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Parameter Name    : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Date Local        : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Time Local        : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Date GMT          : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Time GMT          : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Sample Measurement: list()
##   .. .. ..- attr(*, "class")= chr  "collector_number" "collector"
##   .. ..$ Units of Measure  : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ MDL               : list()
##   .. .. ..- attr(*, "class")= chr  "collector_number" "collector"
##   .. ..$ Uncertainty       : list()
##   .. .. ..- attr(*, "class")= chr  "collector_number" "collector"
##   .. ..$ Qualifier         : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Method Type       : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Method Code       : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ Method Name       : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ State Name        : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   .. ..$ County Name       : list()
##   .. .. ..- attr(*, "class")= chr  "collector_character" "collector"
##   ..$ default: list()
##   .. ..- attr(*, "class")= chr  "collector_guess" "collector"
##   ..- attr(*, "class")= chr "col_spec"
```


```r
head(ozone[, c(6:7, 10)])
```

```
## # A tibble: 6 x 3
##   Latitude Longitude Date.Local
##      <dbl>     <dbl>      <chr>
## 1 30.49748 -87.88026 2014-03-01
## 2 30.49748 -87.88026 2014-03-01
## 3 30.49748 -87.88026 2014-03-01
## 4 30.49748 -87.88026 2014-03-01
## 5 30.49748 -87.88026 2014-03-01
## 6 30.49748 -87.88026 2014-03-01
```



```r
tail(ozone[, c(6:7, 10)])
```

```
## # A tibble: 6 x 3
##   Latitude Longitude Date.Local
##      <dbl>     <dbl>      <chr>
## 1  31.7122 -106.3953 2014-08-31
## 2  31.7122 -106.3953 2014-08-31
## 3  31.7122 -106.3953 2014-08-31
## 4  31.7122 -106.3953 2014-08-31
## 5  31.7122 -106.3953 2014-08-31
## 6  31.7122 -106.3953 2014-08-31
```


```r
head(table(ozone$Time.Local))
```

```
## 
##  00:00  01:00  02:00  03:00  04:00  05:00 
## 369452 370775 360276 354052 358211 384444
```


```r
library(plyr)
```

```
## 
## Attaching package: 'plyr'
```

```
## The following object is masked _by_ '.GlobalEnv':
## 
##     ozone
```

```r
library(dplyr)
```

```
## 
## Attaching package: 'dplyr'
```

```
## The following objects are masked from 'package:plyr':
## 
##     arrange, count, desc, failwith, id, mutate, rename, summarise,
##     summarize
```

```
## The following objects are masked from 'package:stats':
## 
##     filter, lag
```

```
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r
select(ozone, State.Name) %>% unique %>% nrow
```

```
## [1] 53
```


```r
unique(ozone$State.Name)
```

```
##  [1] "Alabama"              "Alaska"               "Arizona"             
##  [4] "Arkansas"             "California"           "Colorado"            
##  [7] "Connecticut"          "Delaware"             "District Of Columbia"
## [10] "Florida"              "Georgia"              "Hawaii"              
## [13] "Idaho"                "Illinois"             "Indiana"             
## [16] "Iowa"                 "Kansas"               "Kentucky"            
## [19] "Louisiana"            "Maine"                "Maryland"            
## [22] "Massachusetts"        "Michigan"             "Minnesota"           
## [25] "Mississippi"          "Missouri"             "Montana"             
## [28] "Nebraska"             "Nevada"               "New Hampshire"       
## [31] "New Jersey"           "New Mexico"           "New York"            
## [34] "North Carolina"       "North Dakota"         "Ohio"                
## [37] "Oklahoma"             "Oregon"               "Pennsylvania"        
## [40] "Rhode Island"         "South Carolina"       "South Dakota"        
## [43] "Tennessee"            "Texas"                "Utah"                
## [46] "Vermont"              "Virginia"             "Washington"          
## [49] "West Virginia"        "Wisconsin"            "Wyoming"             
## [52] "Puerto Rico"          "Country Of Mexico"
```

This last bit of analysis made use of something we will discuss in the next section: external data. We knew that there are only 50 states in the U.S., so seeing 52 state names was an immediate trigger that something might be off. In this case, all was well, but validating your data with an external data source can be very useful.

##Validate With at Least One External Data Source


```r
summary(ozone$Sample.Measurement)
```

```
##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
## -0.00500  0.01900  0.03000  0.03027  0.04100  0.21300
```


```r
quantile(ozone$Sample.Measurement, seq(0, 1, 0.1))
```

```
##     0%    10%    20%    30%    40%    50%    60%    70%    80%    90% 
## -0.005  0.009  0.016  0.022  0.026  0.030  0.035  0.039  0.043  0.050 
##   100% 
##  0.213
```

##Make a plot


```r
par(las = 2, mar = c(10, 4, 2, 2), cex.axis = 0.8)
boxplot(Sample.Measurement ~ State.Name, ozone, range = 0, ylab = "Ozone level (ppm)")
```

![plot of chunk unnamed-chunk-12](images/unnamed-chunk-12-1.png)


```r
library(plotly)
```

```
## Loading required package: ggplot2
```

```
## 
## Attaching package: 'plotly'
```

```
## The following object is masked from 'package:ggplot2':
## 
##     last_plot
```

```
## The following objects are masked from 'package:plyr':
## 
##     arrange, mutate, rename, summarise
```

```
## The following object is masked from 'package:stats':
## 
##     filter
```

```
## The following object is masked from 'package:graphics':
## 
##     layout
```

```r
plot_ly(ozone, y = ~Sample.Measurement, x = ~State.Name, type = "box")
```

![plot of chunk unnamed-chunk-13](images/unnamed-chunk-13-1.png)

##Returning to the original question (Easy Solution First)


```r
library(maps)
```

```
## 
## Attaching package: 'maps'
```

```
## The following object is masked _by_ '.GlobalEnv':
## 
##     ozone
```

```
## The following object is masked from 'package:plyr':
## 
##     ozone
```

```r
{map("state")
abline(v = -100, lwd = 3)
text(-120, 30, "West")
text(-75, 30, "East")}
```

![plot of chunk unnamed-chunk-14](images/unnamed-chunk-14-1.png)


```r
ozone$region <- factor(ifelse(ozone$Longitude < -100, "west", "east"))
group_by(ozone, region) %>% summarize(mean = mean(Sample.Measurement, na.rm = TRUE), median = median(Sample.Measurement, na.rm = TRUE))
```

```
## # A tibble: 2 x 3
##   region       mean median
##   <fctr>      <dbl>  <dbl>
## 1   east 0.02875368  0.029
## 2   west 0.03298395  0.034
```


```r
boxplot(Sample.Measurement ~ region, ozone, range = 0)
```

![plot of chunk unnamed-chunk-16](images/unnamed-chunk-16-1.png)

##Challenge your solution


```r
filter(ozone, State.Name != "Puerto Rico" & State.Name != "Georgia" & State.Name != "Hawaii") %>% group_by(region) %>% summarize(mean = mean(Sample.Measurement, na.rm = TRUE), median = median(Sample.Measurement, na.rm = TRUE))
```

```
## # A tibble: 2 x 3
##   region       mean median
##   <fctr>      <dbl>  <dbl>
## 1   east 0.02884025  0.029
## 2   west 0.03303140  0.034
```
